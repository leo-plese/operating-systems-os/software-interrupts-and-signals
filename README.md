Software Interrupts and Signals. Tasks:

1 Software Signals ("prosti_brojevi.c")

2 Software Interrupts and Signals ("generator_prekida.c", "obrada.c")

Implemented in C using <signal.h> header file.

My lab assignment in Operating Systems, FER, Zagreb.

Task descriptions in "TaskSpecification_InterruptsAndSignals.pdf" and "TaskSpecification_Signals.pdf".

Created: 2018
