#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

int pid = 0;
int sig[] = {SIGUSR1, SIGUSR2, SIGHUP, SIGQUIT};

void prekidna_rutina(int sig)
{
	kill(pid, SIGKILL);
	exit(0);
}

int main(int argc, char *argv[])
{
	int rbrSig, i;

	pid = atoi(argv[1]);

	sigset(SIGINT, prekidna_rutina);

	srand((unsigned) time(NULL));

	while (1) {
		i = sleep((rand() % 3) + 3);
		sleep(i);
		rbrSig = rand() % 4;
		kill(pid, sig[rbrSig]);
			
	}

	return 0;
}
