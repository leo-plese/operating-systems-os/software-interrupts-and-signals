#include <signal.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/time.h>

#define N 6

int OZNAKA_CEKANJA[N];
int PRIORITET[N];
int TEKUCI_PRIORITET = 0;
int proslo = 0;
int prekid = 0;

int sig[] = {SIGUSR1, SIGUSR2, SIGHUP, SIGQUIT, SIGINT};


void ispisVar()
{
	int i;

	printf("      O_CEK[");

	for (i = 0; i < 6; i++) {
		printf("%d ", OZNAKA_CEKANJA[i]);
	}

	printf("]  TEK_PRIOR=%d  ", TEKUCI_PRIORITET);
	printf("PRIOR[");

	for (i = 0; i < 6; i++) {
		printf("%d ", PRIORITET[i]);
	}

	printf("]\n");
}

void periodicki_posao()
{
	static int i = 1;

	if (TEKUCI_PRIORITET)
		return;
	
	if (i >= 10)
		proslo = 1;

	printf ( "%2d  -  -  -  -  -", i++);
	ispisVar();
}

void zabrani_prekidanje()
{
	int i;

	for (i = 0; i < 5; i++)
		sighold(sig[i]);
}

void dozvoli_prekidanje()
{
	int i;

	for (i = 0; i < 5; i++)
		sigrelse(sig[i]);
}

void obrada_signala(int i)
{
	int j;
	char *strFormat;

	prekid = 1;

	switch (i) {
		case 1:
			strFormat = " -  %c  -  -  -  -";
			break;
		case 2:
			strFormat = " -  -  %c  -  -  -";
			break;
		case 3:
			strFormat = " -  -  -  %c  -  -";
			break;
		case 4:
			strFormat = " -  -  -  -  %c  -";
			break;
		case 5:
			strFormat = " -  -  -  -  -  %c";
			break;
	}
	
	printf(strFormat, 'P');
	ispisVar();

	for (j = 0; j <= 4; j++) {
		sleep(1);
		printf(strFormat, j+1+'0');
		ispisVar();		
	}

	printf(strFormat, 'K');
	ispisVar();
}

void prekidna_rutina(int sig)
{
	int n = -1, j, x;

	zabrani_prekidanje();

	switch (sig) {
		case SIGUSR1:
			n = 1;
			printf(" -  X  -  -  -  -");
			break;
		case SIGUSR2:
			n = 2;
			printf(" -  -  X  -  -  -");
			break;
		case SIGHUP:
			n = 3;
			printf(" -  -  -  X  -  -");
			break;
		case SIGQUIT:
			n = 4;
			printf(" -  -  -  -  X  -");
			break;
		case SIGINT:
			n = 5;
			printf(" -  -  -  -  -  X");
			break;
	}

	ispisVar();
	OZNAKA_CEKANJA[n]++;
	
	do {
		x = 0;

		for (j = TEKUCI_PRIORITET + 1; j < N; ++j) {
			if (OZNAKA_CEKANJA[j] > 0)
				x = j;	
		}

		if (x > 0) {
			OZNAKA_CEKANJA[x]--;
			PRIORITET[x] = TEKUCI_PRIORITET;
			TEKUCI_PRIORITET = x;
			dozvoli_prekidanje();
			obrada_signala(x);
			zabrani_prekidanje();
			TEKUCI_PRIORITET = PRIORITET[x];
			PRIORITET[x] = 0;
		}
	} while (x > 0);

	dozvoli_prekidanje();
}

int main(void)
{
	struct itimerval t;

	printf("Proces obrade prekida, PID=%ld\n", getpid());
	printf("GP S1 S2 S3 S4 S5\n-----------------\n");
	
	sigset(SIGALRM, periodicki_posao);
	t.it_value.tv_sec = 1;
	t.it_value.tv_usec = 0;
	t.it_interval.tv_sec = 1;
	t.it_interval.tv_usec = 0;
	
	setitimer(ITIMER_REAL, &t, NULL);

	sigset(SIGUSR1, prekidna_rutina);
	sigset(SIGUSR2, prekidna_rutina);
	sigset(SIGHUP, prekidna_rutina);
	sigset(SIGQUIT, prekidna_rutina);
	sigset(SIGINT, prekidna_rutina);

	while (!proslo || prekid == 1)
		pause();
	
	printf ("Zavrsio osnovni program\n");

	return 0;
}
